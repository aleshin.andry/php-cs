## О проекте
Это кастомная конфигурация [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer).

## Предварительная настройка
Для запуска необходимо скачать [**phpcs.phar**](https://github.com/squizlabs/PHP_CodeSniffer/releases).

## Использование

Запуск проверки файлов
```
./phpcs.phar --extensions=php --standard=./rules.xml,PSR12 --ignore="vendor/*,resources/views/*" ./
```